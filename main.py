#!env/bin/python


from fastapi import FastAPI
from fastapi.params import Body

from pydantic import BaseModel
from datetime import date, datetime
from datetime import timedelta

from pprint import pprint



app = FastAPI()


class RegisterApi(BaseModel):
    '''
    '''
    user_name: str = 'Vivek'
    dob: date = date(1980, 4, 17)
    gender: str 
    address : dict

@app.get('/')
async def root():
    '''
    '''
    return {'page': 'home'}


@app.get('/about')
async def about_page():
    '''
    '''
    return {'page' : 'about'}


@app.get('/contact')
async def contact_page():
    '''
    '''
    return {'page' : 'contact us'}


@app.post('/register')
async def register_user(new_user: RegisterApi):
    '''
    '''
    print(type(new_user), '\n', new_user, '\n\n\n')
    print(type(new_user.json()), '\n', new_user.json(), '\n\n\n')
    print(type(new_user.dict()))
    pprint(new_user.dict())
    name = new_user.user_name
    td = date.today() - new_user.dob
    print(td.days // 365)
    print(f'Name:{name} registered to site')
    return  {'status': 'success',
            'name' : name}
